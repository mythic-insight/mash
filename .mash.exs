defmodule MashConfig do
  @behaviour Mash.Config

  import Mash.Helpers

  def jobs do
    [
      %{
        name: :restore_cache,
        if: fn _jobs ->
          System.get_env("CI")
        end,
        run: restore_cache()
      },
      %{
        name: :compile_dev,
        needs: [restore_cache: :optional],
        run: mix("do", ["deps.compile,", "compile"], env: [{"MIX_ENV", "dev"}])
      },
      %{
        name: :test,
        needs: [restore_cache: :optional],
        run: mix("test")
      },
      %{
        name: :credo,
        needs: [restore_cache: :optional],
        run: mix("credo", ["--all"])
      },
      %{
        name: :save_cache,
        needs: [:test, :compile_dev],
        run: save_cache()
      },
      %{
        name: :function_test,
        run: fn _io_pid ->
          IO.puts("This line should be captured.")
        end
      },
      %{
        if: false,
        run: fn _io_pid ->
          {:error, "This job should not run."}
        end
      }
    ]
  end
end
