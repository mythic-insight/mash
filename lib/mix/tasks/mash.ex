defmodule Mix.Tasks.Mash do
  @shortdoc "Run Mash jobs."

  @moduledoc """
  Task to run Mash jobs.
  """
  use Mix.Task

  def run(_args) do
    {:ok, _pid} = Mash.Runner.start_link()

    receive do
      {:result, []} ->
        :ok

      {:result, failures} ->
        failure_list = Enum.map_join(failures, "\n", fn %{name: name} -> "- #{name}" end)
        failure_count = Enum.count(failures)
        Mix.raise("#{failure_count} job(s) failed.\n#{failure_list}", exit_status: 1)
    end
  end
end
