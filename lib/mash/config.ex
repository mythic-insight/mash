defmodule Mash.Config do
  @moduledoc """
  Configuration module backed by an agent.
  """

  use Agent

  alias Mash.Job

  @callback jobs() :: [%{name: atom(), run: function()}]

  def start_link do
    case Agent.start_link(&compile_config_module!/0, name: __MODULE__) do
      {:error, {:already_started, pid}} ->
        {:ok, pid}

      other ->
        other
    end
  end

  def module do
    with_pid(fn pid ->
      Agent.get(pid, & &1)
    end)
  end

  def jobs do
    jobs =
      module().jobs()
      |> List.flatten()
      |> Enum.filter(& &1)
      |> Enum.map(&Job.from_map/1)

    errors =
      jobs
      |> Enum.map(&Job.validate/1)
      |> Enum.reject(fn {status, _result} -> status == :ok end)
      |> Enum.map(&elem(&1, 1))
      |> Enum.with_index()
      |> Enum.map(&{elem(&1, 1), elem(&1, 0)})

    if Enum.any?(errors) do
      raise "Invalid job specfication(s): #{inspect(errors)}"
    else
      jobs
    end
  end

  defp with_pid(fun) do
    case start_link() do
      {:ok, pid} ->
        fun.(pid)

      other ->
        other
    end
  end

  defp compile_config_module! do
    case Code.compile_file(".mash.exs", File.cwd!()) do
      [{mod, _} | _] ->
        mod

      _ ->
        raise "No .mash.exs file found or it does not export a config module."
    end
  end
end
