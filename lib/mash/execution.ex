defmodule Mash.Execution do
  @moduledoc """
  Wraps execution of a job run function in a Task and a StringIO for tracking output.
  """

  def run(%{run: run} = job) do
    {:ok, io_pid} = StringIO.open("")

    task =
      Task.async(fn ->
        Process.group_leader(self(), io_pid)

        {%{job | io_pid: io_pid}, run.(io_pid)}
      end)

    {task, io_pid}
  end
end
