defmodule Mash.Job do
  @moduledoc """
  Structure that represents the configuration and status of one Mash job.

  Keys:
  - `:name`: an atom which is the name of the job. It should be unique in the job list.
  - `:if`: either a function w/ arity 1 or a plain value. The job will be run if the value is truthy \
          or the function returns true. Otherwise it will be skipped. The argument to an if function is the runner's \
          job list.
  - `:needs`: a list of job name atoms which are required before running this job. If a dependency fails or is skipped, \
             this job will be skipped. You can also include `{job_name, :optional}` to indicate an optional dependency. \
             Optional dependencies do not block a job from running if they are skipped, only if they fail.
  - `:run`: a function with arity 1 that is the code to be executed for the job. See `Mash.Helpers` for shortcut functions.\
           The argument to the run function is the PID of the IO device which is capturing the output of this job. You\
           probably don't need this, but you might need it for certain libaries that need to be passed an explicit IO PID.
  - `:env`: an array of `{string, string}` pairs which represent ENV variables (name first, then value).
  """

  defstruct name: nil, if: true, needs: [], run: nil, env: [], state: nil, task: nil, io_pid: nil

  def from_map(map) do
    struct(__MODULE__, map)
  end

  def validate(%__MODULE__{run: run, name: name} = job) do
    errors =
      Enum.reject(
        [
          if(!is_function(run, 1), do: "run is required and must be a function with arity 1"),
          if(!is_atom(name), do: "name is required and must be an atom")
        ],
        &is_nil(&1)
      )

    if errors == [] do
      {:ok, job}
    else
      {:error, errors}
    end
  end
end
