defmodule Mash.Helpers do
  @moduledoc """
  Helpers that provide a simple way to write Mash configs.
  """

  @doc """
  Create a run function for a job that executes a mix task with name `task_name` and the arguments `args`.

  Available opts:
  - `:env` -- a list of {key, value} pairs that will be used as environment variables.
  """
  @spec mix(String.t(), [String.t()], Keyword.t()) :: function()
  def mix(task_name, args \\ [], opts \\ []) do
    env = Keyword.get(opts, :env, [])

    mix_env =
      Enum.find_value(env, fn {key, value} -> key == "MIX_ENV" && value end) ||
        Mix.Task.preferred_cli_env(task_name) ||
        Mix.env() ||
        :dev

    mix_env =
      case mix_env do
        env when is_binary(env) ->
          env

        env ->
          Atom.to_string(env)
      end

    fn io_pid ->
      cmd("mix", [task_name | args], io_pid, [{"MIX_ENV", mix_env} | env])
    end
  end

  @doc """
  Create a run function which executes the script/binary named `command` with the arguments `args`.

  Available opts:
  - `:env` -- a list of {key, value} pairs that will be used as environment variables.
  """
  @spec shell(String.t(), [String.t()], Keyword.t()) :: function()
  def shell(command, args \\ [], opts \\ []) do
    env = Keyword.get(opts, :env, [])

    fn io_pid ->
      cmd(command, args, io_pid, env)
    end
  end

  @doc """
  Save a cache file as a gzipped tar archive. We use tar because it preserves timestamps which are used
  by Elixir for determining "staleness" of compilation.

  - `name` is the base name of the archive to save (default: `".mash-cache"`)
  - `files` is the list of files/directories to save in the archive (default:` ["deps", "_build"]`)
  """
  @spec save_cache(String.t(), [String.t()]) :: function()
  def save_cache(name \\ ".mash-cache", files \\ ["deps", "_build"]) do
    fn io_pid ->
      cmd("tar", ["-czpf", "#{name}.tar.gz" | files], io_pid, [])
    end
  end

  @doc """
  Restore a cache file.

  - `name` is the base name of the archive to save (default: ".mash-cache")
  """
  @spec restore_cache(String.t()) :: function()
  def restore_cache(name \\ ".mash-cache") do
    fn io_pid ->
      path = "#{name}.tar.gz"

      if File.exists?(path) do
        cmd("tar", ["-xzf", "#{name}.tar.gz", "--atime-preserve"], io_pid, [])
      else
        IO.puts(IO.ANSI.yellow() <> "Warning: cache file #{path} does not exist." <> IO.ANSI.reset())
      end
    end
  end

  defp cmd(command, args, io_pid, env) do
    exec_string = ~s(-c #{command} #{Enum.join(args, " ")})

    System.cmd("script", ["/dev/null", "-eqf", exec_string],
      into: IO.stream(io_pid, :line),
      env: env,
      parallelism: true,
      stderr_to_stdout: true
    )
  end
end
