defmodule Mash.MixProject do
  use Mix.Project

  def project do
    [
      app: :mash,
      version: "0.3.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [
        main: "Mash"
      ],
      source_url: "https://gitlab.com/mythic-insight/mash",
      package: [
        description: "A dead simple tool for defining and running CI pipelines in Elixir.",
        licenses: ["MIT"],
        links: %{"Gitlab" => "https://gitlab.com/mythic-insight/mash"}
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.30.4", only: :dev},
      {:credo, "~> 1.7"},
      {:styler, "~> 0.8.4"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
