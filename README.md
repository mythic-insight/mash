Mash is a dead simple tool for defining and running CI pipelines in Elixir.

## Dependencies

- Elixir
- Mix
- The unix `script` utility

## Installation

Add mash to your dependencies like so:

```elixir
def deps do
  [
    {:mash, "~> 0.1.2"}
  ]
end
```

## Configuring pipelines

To configure your job pipeline, create a `.mash.exs` file at the root of your project containing a module which implements a `jobs/0` function:

```elixir
defmodule MashConfig do
  import Mash.Helpers

  def jobs do
    [
      %{
        name: :test,
        run: mix("test")
      },
      %{
        name: :credo,
        run: mix("credo", ["--all"])
      },
      %{
        name: :function_example,
        run: fn _io_pid ->
          IO.puts("Hurray!")
        end
      }
    ]
  end
end
```

Each entry in the jobs list must, at a minimum, have two keys:

- `:name` -- the name of the job which must be a unique atom
- `:run` -- a function which defines the work for the job to do. We recommend using the helper functions from Mash.Helpers to define your run function, as they ensure that your function handles setup and teardown correctly for many common scenarios, e.g.:
    - `Mash.Helpers.mix(task_name, args)` -- execute a mix task with the given `task_name` and `args`.
    - `Mash.Helpers.shell(command, args)` -- run an executable/script named `command` with `args`.
    - Or you can pass a function with arity 1.
      - The function should return `:ok` or `{:ok, value}` if the job succeeds, or `{:error, error}` if the job fails.
      - The argument passed to your function will be the PID of the IO device which is capturing IO. Since we make this PID the "group leader" for your job automatically, you can probably ignore this argument, but you may need to for interfacing with certain libraries that
      require you to pass an io device PID directly.

Optionally, a job may also contain a key `needs` whose value is an array of other jobs that must pass before this job will be run.

See [the docs for Mash.Job](https://hexdocs.pm/mash/Mash.Job.html) for more details, including conditional execution.

## Running

```shell
mix mash
```

## Full Documentation

Full documentation is available on [HexDocs](https://hexdocs.pm/mash).
